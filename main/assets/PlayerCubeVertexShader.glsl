#version 100

uniform mat4 uLocalMatrix;

uniform mat4 uMVPMatrices[3];
uniform vec4 mainLightPosition;
attribute vec4 vPosition;
attribute vec4 vNormal;
varying vec4 toLight;
varying vec4 wNormal;

void main() {
   // wNormal = uMVPMatrices[0] * uLocalMatrix * vNormal;
    vec4 wPosition = uMVPMatrices[0] * uLocalMatrix * vPosition;

   // toLight = normalize(mainLightPosition - wPosition);

    gl_Position = uMVPMatrices[2]*uMVPMatrices[1]*wPosition;
}