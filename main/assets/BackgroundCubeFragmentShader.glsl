#version 100

precision mediump float;

uniform vec4 vColor;
uniform int isItLight;
varying vec4 toLight;
varying vec4 wNormal;

void main() {
    if( isItLight == 1 ){
        gl_FragColor = vColor;
    } else {
        float outColorCoef = max (dot( toLight, normalize(wNormal)), 0.2);
        vec3 lightedColor = vColor.rgb * outColorCoef;
        lightedColor.b = lightedColor.b < 0.2 ? 0.2 : lightedColor.b;
        lightedColor.g = lightedColor.g < 0.11 ? 0.11 : lightedColor.g;
        lightedColor.r = lightedColor.r < 0.11 ? 0.11 : lightedColor.r;
        gl_FragColor = vec4(lightedColor, vColor.a);
    }
}