package com.hulponot.games.fallingsquare;

import android.app.Activity;
import android.opengl.GLES10;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.opengl.GLES31;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.SystemClock;
import android.util.Log;
import android.widget.TextView;

import com.hulponot.games.fallingsquare.cube.BackgroundCubeMovement;
import com.hulponot.games.fallingsquare.cube.BackgroundCubeRenderer;
import com.hulponot.games.fallingsquare.cube.PlayerCubeMovement;
import com.hulponot.games.fallingsquare.cube.PlayerCubeRenderer;
import com.hulponot.games.fallingsquare.levels.LevelBlocks;
import com.hulponot.games.fallingsquare.levels.LevelBlocksInternal;
import com.hulponot.games.fallingsquare.levels.LevelBlocksOne;
import com.hulponot.games.fallingsquare.levels.LevelBlocksTutorial;
import com.hulponot.games.fallingsquare.levels.SimpleRoomRenderer;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES20.GL_FRAMEBUFFER;
import static android.opengl.GLES20.GL_UNSIGNED_BYTE;
import static android.opengl.GLES20.glHint;

/**
 * Created by hulpo on 6/3/2017.
 */

public class MyGLRenderer implements GLSurfaceView.Renderer {
    public static final float MIN_SPEED = 0.7f;
    private final float TIME_COEF = 0.04f;
    private final float TOTAL_DESCEND_START = 5.0f;
    private final float ACCELERATION_DESC = 0.8f;
    private final float HOSRIZONTAL_BORDER = 8 * SimpleRoomRenderer.cubeScale / 2 - PlayerCubeMovement.PLAYER_WIDTH / 2;
    private final float HORIZONTAL_COEF = 0.1f;
    private final float VERTICAL_COEF = 0.1f;
    private final float TIME_ROTATION_COEF = 10f;
    private static final float ROTATION_STEP = 2.0f;
    private int deathCounter;

    private float total_descend = TOTAL_DESCEND_START;
    private float horizontal_offset = 0.0f;
    private volatile boolean horizontalDirection = false; // <- false true ->
    private Cube player;
    private BackgroundCubeRenderer backgroundCubeRenderer;
    private PlayerCubeRenderer playerCubeRenderer;
    private float speed = MIN_SPEED;
    private float acceleration = 0.05f;
    volatile private boolean isAccelerating = false;
    private volatile long prevTime;
    private float timeDiff = 0.0f;
    private LevelBlocks level;

    private List<Cube> mBackgroundCubes = new LinkedList<>();
    private int speedo;
    private Activity activity;
    private boolean isTutorial;
    private boolean isInternal;
    private volatile float mAngle;
    private volatile float mExcitingAngle = 0.0f;
    private volatile float mTimeAngle;

    private final float[] mMVPMatrices = new float[16 * 3];
    private final float[] eyePosition = new float[4];
    private float[] lookAtCenter = new float[]{0, 0, 0};
    private float needRotate = 0.0f;

    private volatile boolean onPause;
    private boolean stageComplete = false;

    public MyGLRenderer(Activity activity, boolean isTutorial, boolean isInternal) {
        super();

        this.isTutorial = isTutorial;
        this.isInternal = isInternal;
        this.activity = activity;
    }

    public void addRotate(float rotation) {
        needRotate += rotation;
    }

    public boolean getIsComplete() {
        return stageComplete;
    }

    public boolean getDirection() {
        return horizontalDirection;
    }

    public void setDirection(boolean dir) {
        horizontalDirection = dir;
    }

    public float[] getEyePosition() {
        return eyePosition;
    }

    public void accelerate(boolean isAccelerating) {
        this.isAccelerating = isAccelerating;
    }

    public Cube getPlayer() {
        return player;
    }

    public int getRoomNum() {
        return level.getRoomNum();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        deathCounter = 0;
        GLES10.glEnable(GLES10.GL_BLEND);
        GLES10.glBlendFunc(GLES10.GL_SRC_ALPHA, GLES10.GL_ONE_MINUS_SRC_ALPHA);

        backgroundCubeRenderer = new BackgroundCubeRenderer();
        playerCubeRenderer = new PlayerCubeRenderer();
        player = new Cube(
                new PlayerCubeMovement(new float[]{0, 0, 0}),
                playerCubeRenderer,
                this,
                new float[]{0.6f, 0.3f, 0.2f, 1.0f}
        );
        GLES20.glClearColor(0.12f, 0.12f, 0.17f, 1.0f);

        float step = (float) Math.PI / 12.0f;
        float hStep = 0.2f * 5.0f;
        float radius = 50.0f;
        for (float theta = -(float) Math.PI * 5.0f, h = -40.0f; theta < Math.PI * 8.0f; theta += step, h += hStep) {
            float center[] = new float[3];
            center[0] = radius * (float) (Math.cos(theta));
            center[1] = h;
            center[2] = radius * (float) (Math.sin(theta));
            mBackgroundCubes.add(new Cube(new BackgroundCubeMovement(center), backgroundCubeRenderer, this));
        }

        for (int i = 0; i < 4; i++)
            eyePosition[i] = 0.0f;
        eyePosition[1] = 2;
        eyePosition[2] = 20.0f;
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        prevTime = SystemClock.uptimeMillis();

        if (isTutorial)
            level = new LevelBlocksTutorial(this, backgroundCubeRenderer);
        else if (isInternal) {
            level = new LevelBlocksInternal(this, backgroundCubeRenderer);

        } else
            level = new LevelBlocksOne(this, backgroundCubeRenderer, ((GameActivity) activity).getLevelStart());

        if (!isInternal) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) activity.findViewById(R.id.maxSpeed)).setText(String.valueOf(level.getSpeedForWin()));
                }
            });
        }

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        float ratio = (float) width / height;
        Matrix.perspectiveM(mMVPMatrices, 16 * 2, 60, ratio, 0.1f, 100.0f);

        if (!isInternal() && !isTutorial) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((GameActivity) activity).showLevel(level.getLevelNum());
                }
            });
        }
    }


    @Override
    public void onDrawFrame(GL10 gl) {

        GLES20.glBindFramebuffer(GL_FRAMEBUFFER, 0);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        long time = SystemClock.uptimeMillis();
        if (onPause)
            return;
        if (!stageComplete) {
            timeDiff = (time - prevTime) * TIME_COEF;

            if (isAccelerating) {
                speed += timeDiff * acceleration;
            } else {
                if (speed > MIN_SPEED) {
                    speed -= timeDiff * acceleration * ACCELERATION_DESC;
                }
            }

            if (LevelBlocks.isGameOver()) {
                timeDiff = 0;
                speed = 0.0f;
                if (isInternal) {
                    ((InternalActivity) activity).updateScores();
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((InternalActivity) activity).updateMaxScoreView();
                        }
                    });
                }
            } else if (isInternal) {

            } else if (speedo != (int) (speed * 3)) {
                speedo = (int) (speed * 3);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((TextView) activity.findViewById(R.id.tmpSpeed)).setText(String.valueOf(speedo));
                    }
                });
                isWin(speedo);
            }
            //Log.d("RENDERING", "onDrawFrame: " + horizontal_offset + " TIME DIFF: " + timeDiff + " speed: " + speed);
            switch (((PlayerCubeMovement) player.getMovement()).getState()) {
                case IDLE:
                case RROTATING:
                    horizontal_offset += (horizontalDirection ? speed : -speed) * HORIZONTAL_COEF * timeDiff;
                    if (HOSRIZONTAL_BORDER <= horizontal_offset) {
                        horizontal_offset = HOSRIZONTAL_BORDER;
                        horizontalDirection = false;
                    } else if ((-HOSRIZONTAL_BORDER) >= horizontal_offset) {
                        horizontal_offset = -HOSRIZONTAL_BORDER;
                        horizontalDirection = true;
                    }
                    break;
                case MOVING:
                case ROTATING:
                    total_descend += speed * timeDiff;
                    break;
            }

            mTimeAngle = TIME_ROTATION_COEF * (float) Math.sin(0.001f * ((int) time));
            if (LevelBlocks.isGameOver())
                mTimeAngle += 0.f;

            mExcitingAngle += speed;

            if (needRotate > ROTATION_STEP) {
                mAngle += needRotate / 100.0f;
                needRotate -= needRotate / 100.0f;
            } else if (needRotate > 0) {
                mAngle += ROTATION_STEP;
                needRotate = 0;
            }
            Matrix.setLookAtM(mMVPMatrices, 16,
                    eyePosition[0], eyePosition[1], eyePosition[2],
                    lookAtCenter[0], lookAtCenter[1], lookAtCenter[2],
                    0f, 1.0f, 0.0f);
        } else {
            player.getMovement().translate(-1f, 0.0f, 0);
            if (lookAtCenter[1] > -10) {
                lookAtCenter[1] -= 0.1;
            }

            mTimeAngle = TIME_ROTATION_COEF * (float) Math.sin(0.001f * ((int) time));

            Matrix.setLookAtM(mMVPMatrices, 16,
                    eyePosition[0], eyePosition[1], eyePosition[2],
                    lookAtCenter[0], lookAtCenter[1], lookAtCenter[2],
                    0f, 1.0f, 0.0f);
        }
        GLES20.glUseProgram(playerCubeRenderer.getmProgram());
        //player
        Matrix.setRotateM(mMVPMatrices, 0, mAngle, 0.0f, 1.0f, 0.0f);
        Matrix.translateM(mMVPMatrices, 0, horizontal_offset, 0, 0);
        player.draw(mMVPMatrices, time, false);


        GLES20.glUseProgram(backgroundCubeRenderer.getmProgram());
        //background cubes
        Matrix.setRotateM(mMVPMatrices, 0, mAngle + mTimeAngle + mExcitingAngle, 0.0f, 1.0f, 0.0f);
        for (Cube cube : mBackgroundCubes) {
            cube.draw(mMVPMatrices, time, false);
        }

        //level cubes
        Matrix.setRotateM(mMVPMatrices, 0, mAngle, 0.0f, 1.0f, 0.0f);
        Matrix.translateM(mMVPMatrices, 0, 0, total_descend * VERTICAL_COEF, 0);
        if (!LevelBlocks.isGameOver())
            level.move(total_descend * VERTICAL_COEF, horizontal_offset);
        level.draw(mMVPMatrices, time);

        prevTime = time;
    }

    public static int loadShaderFromFile(int type, String filePath) {
        try {
            String shaderString = getStringFromFile(filePath);
            return loadShader(type, shaderString);
        } catch (Exception e) {
            return -1;
        }
    }

    public static int loadShader(int type, String shaderCode) {

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        int params[] = new int[2];
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, params, 0);
        GLES20.glGetShaderiv(shader, GLES20.GL_INFO_LOG_LENGTH, params, 1);

        if (params[0] == GLES20.GL_FALSE) {
            String errmsg = GLES20.glGetShaderInfoLog(shader);
            Log.e("SHADER ERROR", "loadShader: " + errmsg + shaderCode);
            System.exit(-1);
        }
        return shader;
    }

    public void restart() {
        if (!isInternal() && !isTutorial) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((GameActivity) activity).showWin(false);
                }
            });
        }
        mAngle = 0;
        needRotate = 0;
        speed = MIN_SPEED;
        horizontal_offset = 0;
        horizontalDirection = true;
        total_descend = TOTAL_DESCEND_START;
        level.restart();
    }

    public static String getStringFromFile(String filePath) throws Exception {
        StringBuilder buf = new StringBuilder();
        InputStream is = MyApplication.getAppContext().getAssets().open(filePath);
        BufferedReader in =
                new BufferedReader(new InputStreamReader(is, "UTF-8"));
        String str;
        while ((str = in.readLine()) != null) {
            buf.append(str).append("\n");
        }
        in.close();
        return buf.toString();
    }

    private void isWin(int speed) {
        if (level.getSpeedForWin() <= speed) {
            if (isTutorial) {
                stageComplete = true;
            } else {
                stageComplete = true;
                if (!isInternal() && !isTutorial) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((GameActivity) activity).showWin(true);
                        }
                    });
                }
            }
        }
    }

    public Activity getActivity() {
        return activity;
    }

    public LevelBlocks getLevel() {
        return level;
    }

    public void nextLevel() {
        mAngle = 0;
        needRotate = 0;
        stageComplete = false;
        horizontal_offset = 0;
        total_descend = TOTAL_DESCEND_START;
        player.getMovement().translateTo(0, 0, 0);
        if (!isInternal && !isTutorial) {
            ((LevelBlocksOne) level).nextLevel();
            Log.d("HIGHLEVEL", "nextLevel: " + level.getLevelNum() + " (+2)");
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((GameActivity) activity).openLevel(level.getLevelNum() + 1);
                    ((GameActivity) activity).showLevel(level.getLevelNum());
                    ((GameActivity) activity).showWin(false);
                }
            });
        }
        lookAtCenter[1] = 0;
        speed = MIN_SPEED;
    }

    public boolean isInternal() {
        return isInternal;
    }

    public float getSpeed() {
        return speed;
    }
    public boolean isPlayerSafe(){return level.getIsSafe();}
    public int getDeathCounter(){return deathCounter;}
    public void incDeathCounter(){deathCounter++;}

    public boolean isOnPause() {
        return onPause;
    }

    public void setOnPause(boolean onPause) {
        prevTime = SystemClock.uptimeMillis();
        this.onPause = onPause;
        Log.d("PAUSE STATUS", "setOnPause: " + onPause);
    }
}
