package com.hulponot.games.fallingsquare;

import com.hulponot.games.fallingsquare.cube.CubeMovement;
import com.hulponot.games.fallingsquare.cube.CubeRenderer;

/**
 * Created by hulpo on 6/3/2017.
 */

public class Cube {

    private static MyGLRenderer renderer;
    private CubeMovement movement;
    private CubeRenderer cubeRenderer;

    private float color[];


    public CubeMovement getMovement(){return movement;}
    public Cube(CubeMovement cMovement, CubeRenderer cubeRenderer,MyGLRenderer cRenderer) {
        renderer = cRenderer;
        movement = cMovement;
        this.cubeRenderer = cubeRenderer;
        color = new float[]{(float)Math.random(),(float)Math.random(),(float)Math.random(),1.0f};
    }
    public Cube(CubeMovement cMovement, CubeRenderer cubeRenderer, MyGLRenderer cRenderer, float[] color) {
        renderer = cRenderer;
        movement = cMovement;
        this.color = color;
        this.cubeRenderer = cubeRenderer;
    }

    public void rotate(float angle){
           movement.rotate(angle);
    }
    public void draw(float mvpMatrices[],long time, boolean isItLight) {
        cubeRenderer.draw(mvpMatrices, time, movement, renderer, color,isItLight);
    }
    public void draw(float mvpMatrices[], float color[], long time, boolean isItLight) {
        cubeRenderer.draw(mvpMatrices, time, movement, renderer, color,isItLight);
    }
    public void setColor(float newColor[]){color = newColor;}
    public void setColorPart(int index, float value){if (color.length > index) color[index] = value;}
}
