package com.hulponot.games.fallingsquare.cube;

/**
 * Created by hulpo on 6/12/2017.
 */

public abstract class CubeMovement {

    protected float center[] = new float[3];
    protected float scaleFactor[] = new float[3];
    protected float TRSmat[] = new float[16];
    protected float angleWeights[] = new float[4];

    public CubeMovement(float center[]){
        this.center = center;
    }
    abstract public float[] move(long time);

    public float[] getCenter(){ return center; }
    public void rotate(float angle){};
    public void scale (float x, float y, float z){
        scaleFactor[0]=x;
        scaleFactor[1]=y;
        scaleFactor[2]=z;
    }

    public void translateTo (float x, float y, float z){
        center[0] = x;
        center[1] = y;
        center[2] = z;
    }
    public void translate (float x, float y, float z){
        center[0] += x;
        center[1] += y;
        center[2] += z;
    }
}
