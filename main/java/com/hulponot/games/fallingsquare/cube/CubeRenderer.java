package com.hulponot.games.fallingsquare.cube;

import android.opengl.GLES20;
import android.util.Log;

import com.hulponot.games.fallingsquare.MyGLRenderer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

/**
 * Created by hulpo on 6/12/2017.
 */

public abstract class CubeRenderer {
    protected int mPositionHandle;
    protected int mNormalHandle;
    protected int mColorHandle;
    protected int mLightHandle;
    protected int isItLightHandle;
    protected int mLocalMatrixHandle;
    protected int mMVPMatricesHandle;

    protected IntBuffer VBO;
    protected int mProgram = -1;

    public int getmProgram(){
        return mProgram;
    }


    final int COORDS_PER_VERTEX = 3;
    final int COORDS_PER_NORMAL = 4;
    protected final int vertexStride = COORDS_PER_VERTEX * 4 + COORDS_PER_NORMAL * 4 ; // 4 bytes per vertex

    protected float cubeCoords[] = {
            //near
            -0.5f, 0.5f, -0.5f, -0.5f, 0.5f, -0.5f, 0.0f,// top left
            -0.5f, -0.5f, -0.5f, -0.5f, -0.5f, -0.5f, 0.0f, // bottom left
            0.5f, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f, 0.0f, // bottom right
            0.5f, 0.5f, -0.5f, 0.5f, 0.5f, -0.5f, 0.0f,// top right
            //far
            -0.5f, 0.5f, 0.5f, -0.5f, 0.5f, 0.5f, 0.0f,// top left
            -0.5f, -0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.0f, // bottom left
            0.5f, -0.5f, 0.5f, 0.5f, -0.5f, 0.5f, 0.0f, // bottom right
            0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f,  0.0f}; // top right

    protected short drawOrder[] = {
            0, 1, 2, 0, 2, 3, //front
            3, 2, 6, 3, 6, 7,
            0, 3, 7, 0, 7, 4, //top

            7, 6, 5, 7, 5, 4, //back
            1, 5, 6, 1, 6, 2, //bottom
            0, 5, 1, 0, 4, 5

    }; // order to draw vertices
    protected final int indexCount = drawOrder.length;

    public void clearState(){
        if (mProgram != -1) {
            GLES20.glDeleteBuffers(1, VBO);
            GLES20.glDeleteProgram(mProgram);
            mProgram = -1;
        }
    }
    CubeRenderer(String vertexShaderPath, String fragmentShaderPath){
        ShortBuffer drawListBuffer;
        FloatBuffer vertexBuffer;
        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 4 bytes per float)
                cubeCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(cubeCoords);
        vertexBuffer.position(0);

        // initialize byte buffer for the draw list
        ByteBuffer dlb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 2 bytes per short)
                drawOrder.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);

        int vertexShader = MyGLRenderer.loadShaderFromFile(GLES20.GL_VERTEX_SHADER,
                vertexShaderPath);
        int fragmentShader = MyGLRenderer.loadShaderFromFile(GLES20.GL_FRAGMENT_SHADER,
                fragmentShaderPath);

        if (vertexShader < 0 || fragmentShader < 0){
            Log.e("Shader not compiled", "init: ");
            System.exit(-1);
        }
        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();

        // add the vertex shader to program
        GLES20.glAttachShader(mProgram, vertexShader);

        // add the fragment shader to program
        GLES20.glAttachShader(mProgram, fragmentShader);

        // creates OpenGL ES program executables
        GLES20.glLinkProgram(mProgram);

        GLES20.glUseProgram(mProgram);


        ByteBuffer buffers = ByteBuffer.allocateDirect(
                2 * 4);
        VBO =  buffers.asIntBuffer();
        VBO.position(0);
        GLES20.glGenBuffers(2, VBO);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, VBO.get(0));
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, cubeCoords.length*4, vertexBuffer, GLES20.GL_STATIC_DRAW);

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, VBO.get(1));
        GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, drawOrder.length * 2, drawListBuffer, GLES20.GL_STATIC_DRAW);

        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        mNormalHandle = GLES20.glGetAttribLocation(mProgram, "vNormal");

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);


        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        mLightHandle = GLES20.glGetUniformLocation(mProgram, "mainLightPosition");

        mMVPMatricesHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrices");
        mLocalMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uLocalMatrix");

        isItLightHandle = GLES20.glGetUniformLocation(mProgram, "isItLight");

        GLES20.glUseProgram(0);
    }
    abstract public void draw(float mvpMatrices[], long time, CubeMovement movement, MyGLRenderer renderer,float color[], boolean isItLight);
}