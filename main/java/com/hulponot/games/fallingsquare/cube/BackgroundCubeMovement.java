package com.hulponot.games.fallingsquare.cube;

import android.opengl.Matrix;

/**
 * Created by hulpo on 6/12/2017.
 */

public class BackgroundCubeMovement extends CubeMovement {

    public BackgroundCubeMovement(float center[]){
        super(center);

        for (int i = 1; i < 4; i++) {
            angleWeights[i] = (float) Math.random() * 2 - 1;
            scaleFactor[i - 1] = 5.0f;
        }
    }

    @Override
    public float[] move(long time) {
        angleWeights[0] = 0.090f * ((int) time);

        Matrix.setIdentityM(TRSmat, 0);

        Matrix.translateM(TRSmat, 0, center[0], center[1], center[2]);
        Matrix.scaleM(TRSmat, 0, scaleFactor[0], scaleFactor[1], scaleFactor[2]);
        Matrix.rotateM(TRSmat,0,angleWeights[0], angleWeights[1], angleWeights[2], angleWeights[3]);

        return TRSmat;
    }
}
