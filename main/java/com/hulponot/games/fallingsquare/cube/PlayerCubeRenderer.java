package com.hulponot.games.fallingsquare.cube;

import android.opengl.GLES20;

import com.hulponot.games.fallingsquare.MyGLRenderer;

/**
 * Created by hulpo on 6/12/2017.
 */

public class PlayerCubeRenderer extends CubeRenderer {
    public PlayerCubeRenderer() {
        super("PlayerCubeVertexShader.glsl","PlayerCubeFragmentShader.glsl");
    }

    @Override
    public void draw(float[] mvpMatrices, long time, CubeMovement movement, MyGLRenderer renderer, float[] color, boolean isItLight) {
        GLES20.glUniformMatrix4fv(mLocalMatrixHandle, 1, false, movement.move(time), 0);
        GLES20.glUniformMatrix4fv(mMVPMatricesHandle, 3, false, mvpMatrices, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, VBO.get(0));

        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, 0);

        GLES20.glUniform4fv(mColorHandle, 1, color, 0);
        GLES20.glUniform4fv(mLightHandle, 1, renderer.getEyePosition(), 0);

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, VBO.get(1));
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indexCount, GLES20.GL_UNSIGNED_SHORT, 0);

        GLES20.glDisableVertexAttribArray(mPositionHandle);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
    }
}
