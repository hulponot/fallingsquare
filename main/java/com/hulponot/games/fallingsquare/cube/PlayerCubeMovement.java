package com.hulponot.games.fallingsquare.cube;

import android.opengl.Matrix;

/**
 * Created by hulpo on 6/12/2017.
 */

public class PlayerCubeMovement extends CubeMovement {
    private final float ROTATION_SPEED = 1.5f;
    public static final float PLAYER_WIDTH = 2;
    public enum State {
        ROTATING, IDLE, MOVING, RROTATING
    }
    private State state = State.IDLE;
    public State getState(){
        return state;
    }

    private volatile float needRotate = 0.0f;

    public PlayerCubeMovement(float[] center) {
        super(center);

        for (int i = 1; i < 4; i++) {
            angleWeights[i] = 0.0f;
            scaleFactor[i - 1] = 1.0f;
        }
        angleWeights[3] = 1.0f;
        scaleFactor[2] = 0.5f;
        scaleFactor[1] = 0.2f;
        scaleFactor[0] = PLAYER_WIDTH;
    }

    public void rotate(float angle){
        if (angle > 0 && (state == State.IDLE || state == State.RROTATING ) ){
            state = State.ROTATING;
            needRotate += angle;
        } else if (angle < 0 && (state == State.MOVING || state == State.ROTATING )){
            state = State.RROTATING;
            needRotate += angle;
        }
    }
    @Override
    public float[] move(long time) {

        Matrix.setIdentityM(TRSmat, 0);

        if ( needRotate != 0 ){
            angleWeights[0] += needRotate/ROTATION_SPEED;
            needRotate -= needRotate/ROTATION_SPEED;
            if ( needRotate > 0 && needRotate < 2 ){
                angleWeights[0] += needRotate;
                needRotate = 0;
                state = State.MOVING;
            } else if ( needRotate < 0 && needRotate > -2){
                angleWeights[0] += needRotate;
                needRotate = 0;
                state = State.IDLE;
            }
        }
        Matrix.rotateM(TRSmat,0,angleWeights[0], angleWeights[1], angleWeights[2], angleWeights[3]);
        Matrix.scaleM(TRSmat, 0, scaleFactor[0], scaleFactor[1], scaleFactor[2]);
        Matrix.translateM(TRSmat, 0, center[0], center[1], center[2]);

        return TRSmat;
    }
}
