package com.hulponot.games.fallingsquare;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.hulponot.games.fallingsquare.levels.LevelBlocks;

/**
 * Created by hulpo on 6/3/2017.
 */

public class MyGLSurface extends GLSurfaceView {

    private int adIntencity;
    private final MyGLRenderer mRenderer;
    private float lastX;
    private Activity activity;
    private boolean isTutorial;
    private boolean isInternal;
    private final float threshold = 3;

    public MyGLSurface(Context context) {
        super(context);

        isTutorial = false;
        isInternal = false;

        setEGLContextClientVersion(2);
        activity = (Activity) context;
        mRenderer = new MyGLRenderer(activity, isTutorial, isInternal);
        setRenderer(mRenderer);
    }

    public MyGLSurface(Context context, AttributeSet attrs) {
        super(context, attrs);

        setEGLConfigChooser(new MyEGLConfigChooser());

        isTutorial = attrs.getAttributeBooleanValue("http://schemas.android.com/apk/res-auto", "tutorial", false);
        isInternal = attrs.getAttributeBooleanValue("http://schemas.android.com/apk/res-auto", "internal", false);

        setEGLContextClientVersion(2);
        activity = (Activity) context;
        mRenderer = new MyGLRenderer(activity, isTutorial, isInternal);
        setRenderer(mRenderer);

        adIntencity = 3;
    }

    @Override
    public void onResume() {
        super.onResume();

        mRenderer.setOnPause(false);
    }

    @Override
    public void onPause() {
        super.onPause();

        mRenderer.setOnPause(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if (mRenderer.getPlayer() == null)
            return false;
        if (isTutorial) {
            switch (((TutorialActivity) activity).getCondition()) {
                case 0:
                    //just touch
                    if (e.getAction() == MotionEvent.ACTION_UP)
                        ((TutorialActivity) activity).nextStep();
                    break;
                case 1:
                    //get through cubes
                    if (mRenderer.getRoomNum() > 1)
                        ((TutorialActivity) activity).nextStep();
                    break;
                case 2:
                    //turn left
                    if (e.getX() < lastX) {
                        ((TutorialActivity) activity).nextStep();
                    }
                    break;
                case 3:
                    //win
                    if (mRenderer.getIsComplete()) {
                        ((TutorialActivity) activity).nextStep();
                    }
                    break;
                default:
            }
        }
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastX = e.getX();
                if (LevelBlocks.isGameOver()) {
                    if (mRenderer.getDeathCounter() == adIntencity) {
                        adIntencity *= adIntencity;
                        if (isInternal)
                            ((InternalActivity) activity).showAd();
                        else if (!isTutorial)
                            ((GameActivity) activity).showAd();
                    }
                    mRenderer.restart();
                } else if (mRenderer.getIsComplete()) {
                    mRenderer.nextLevel();
                } else {
                    mRenderer.getPlayer().rotate(90);
                    mRenderer.accelerate(true);
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mRenderer.getIsComplete())
                    break;
                mRenderer.getPlayer().rotate(-90);
                mRenderer.accelerate(false);

            case MotionEvent.ACTION_MOVE:
                if (Math.abs(e.getX() - lastX) > threshold) {
                    //Log.d("MOVE", "onTouchEvent: diff=" + Math.abs(e.getX() - lastX));
                    if (e.getX() < lastX) {
                        mRenderer.setDirection(false);
                    } else if (e.getX() > lastX) {
                        mRenderer.setDirection(true);
                    }
                    lastX = e.getX();
                }
        }
        return true;
    }
}
