package com.hulponot.games.fallingsquare.levels;

import android.app.Activity;
import android.opengl.Matrix;
import android.util.Log;
import android.widget.TextView;

import com.hulponot.games.fallingsquare.InternalActivity;
import com.hulponot.games.fallingsquare.MyGLRenderer;
import com.hulponot.games.fallingsquare.R;
import com.hulponot.games.fallingsquare.cube.BackgroundCubeRenderer;
import com.hulponot.games.fallingsquare.cube.PlayerCubeMovement;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by hulpo on 6/12/2017.
 */

public abstract class LevelBlocks {
    MyGLRenderer renderer;
    BackgroundCubeRenderer backgroundCubeRenderer;
    protected LinkedList<Room> rooms;
    protected int speedForWin;
    protected int tmpRoom = 1;
    protected volatile int levelNum = 0;
    protected Activity activity;
    protected int roomStart;
    private boolean playerSafe = false;

    static volatile protected boolean gameOver = false;
    public static float[] lightColor;
    private float[] deathColor = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
    private float[] safeColor = new float[]{0.8f, 0.8f, 0.0f, 0.1f};
    private float[] unsafeColor = new float[]{0.99f, 0.99f, 0.5f, 0.9f};

    public int getRoomNum(){
        return tmpRoom;
    }
    public int getLevelNum() {
        return levelNum;
    }

    public LevelBlocks(MyGLRenderer renderer, BackgroundCubeRenderer backgroundCubeRenderer, final int speedForWin, int levelNum) {
        this.speedForWin = speedForWin;
        this.renderer = renderer;
        this.backgroundCubeRenderer = backgroundCubeRenderer;
        this.levelNum = levelNum;
        rooms = new LinkedList<>();

        lightColor = unsafeColor;
        gameOver = false;
        activity = renderer.getActivity();
        if (!renderer.isInternal()) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) activity.findViewById(R.id.maxSpeed)).setText(String.valueOf(speedForWin));
                }
            });
        }
        roomStart = 0;
    }

    public void draw(float MVP[], long time) {
        Matrix.translateM(MVP, 0, 0, -roomStart, 0);
        float toReturn  = 0;
        for (int i = tmpRoom-1; i >= 0 && i >= tmpRoom -2; i--){
            rooms.get(i).draw(MVP, time, true);
            Matrix.translateM(MVP, 0, 0, rooms.get(i).getHigh(), 0);
            toReturn += rooms.get(i).getHigh();
        }
        Matrix.translateM(MVP, 0, 0, -toReturn, 0);
        for (int i = tmpRoom; i < tmpRoom + 2 && i < rooms.size(); i++) {
            Matrix.translateM(MVP, 0, 0, -rooms.get(i).getHigh(), 0);
            rooms.get(i).draw(MVP, time, i <= tmpRoom);
        }
    }

    public int getSpeedForWin(){return speedForWin;}

    public void move(float offset, float playerHorizontalOffset) {
        float roomsBottom = roomStart + rooms.get(tmpRoom).getHigh();
        float exit = (-4 + rooms.get(tmpRoom).getExit() + 0.5f) * RoomRenderer.cubeScale;
        if ((playerHorizontalOffset - PlayerCubeMovement.PLAYER_WIDTH / 2) - exit < 0 &&
                (playerHorizontalOffset + PlayerCubeMovement.PLAYER_WIDTH / 2) - exit > 0) {
            playerSafe = true;
            lightColor = safeColor;
        } else {
            playerSafe = false;
            lightColor = unsafeColor;
        }

        if (!playerSafe) {
            if ((roomsBottom - RoomRenderer.cubeScale) < (offset + PlayerCubeMovement.PLAYER_WIDTH / 2)) {
                lightColor = deathColor;
                gameOver = true;
                renderer.incDeathCounter();
                renderer.addRotate(180);
               // Log.d("GAME_OVER", "move: " + tmpRoom + " " + roomsBottom + " offset: " + offset);
            }
        }
        if (offset > roomsBottom) {
            nextRoom();
        }
       // Log.d("ROOMTH", "move: " + tmpRoom + " need" + roomsBottom +  " start:" + roomStart + " offset: " + offset);
    }

    protected void nextRoom(){
        if (tmpRoom + 1 < rooms.size())
            roomStart += rooms.get(tmpRoom).getHigh();
        tmpRoom++;
    }

    static public boolean isGameOver() {
        return gameOver;
    }

    public void restart(){
        tmpRoom = 1;
        roomStart = 0;
        LevelBlocks.setGameOver(false);
        LevelBlocks.lightColor[2] = 0.0f;
    }


    static public void setGameOver(boolean gameOver) {
        LevelBlocks.gameOver = gameOver;
    }
    public boolean getIsSafe(){return playerSafe;}

}
