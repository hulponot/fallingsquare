package com.hulponot.games.fallingsquare.levels;

import com.hulponot.games.fallingsquare.Cube;
import com.hulponot.games.fallingsquare.MyGLRenderer;
import com.hulponot.games.fallingsquare.cube.BackgroundCubeRenderer;
import com.hulponot.games.fallingsquare.cube.CubeMovement;
import com.hulponot.games.fallingsquare.cube.CubeRenderer;

/**
 * Created by hulpo on 6/25/2017.
 */

public class LevelBlocksTutorial extends LevelBlocks {
    public LevelBlocksTutorial(MyGLRenderer renderer, BackgroundCubeRenderer backgroundCubeRenderer) {
        super(renderer, backgroundCubeRenderer, 15, 0);

        RoomRenderer roomRenderer = new SimpleRoomRenderer(renderer, backgroundCubeRenderer);

        rooms.add(new SimpleRoom(roomRenderer, -1, RoomRenderer.cubeScale));
        rooms.add(new SimpleRoom(roomRenderer, 2, 15));
        rooms.add(new SimpleRoom(roomRenderer, 4, 12));
        rooms.add(new SimpleRoom(roomRenderer, -1, 50));

    }
}
