package com.hulponot.games.fallingsquare.levels;

/**
 * Created by hulpo on 6/16/2017.
 */

public class SimpleRoom extends Room {

    @Override
    public void draw(float[] MVP, long time, boolean focus) {
        super.draw(MVP, time, focus);
    }

    public SimpleRoom(RoomRenderer roomRenderer, int exit, float high) {
        super(roomRenderer);

        this.high = high;
        this.exit = exit;
    }

}
