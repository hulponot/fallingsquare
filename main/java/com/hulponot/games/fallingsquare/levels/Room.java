package com.hulponot.games.fallingsquare.levels;

/**
 * Created by hulpo on 6/16/2017.
 */

public abstract class Room {
    protected float angle;
    protected int exit;
    protected float high;
    protected RoomRenderer renderer;

    public void draw(float MVP[], long time, boolean focus){
        renderer.draw(MVP, time, high, focus, exit);
    }
    public Room(RoomRenderer renderer){
        this.renderer = renderer;
    }

    public int getExit(){return exit;}
    public float getHigh(){return high;}
}
