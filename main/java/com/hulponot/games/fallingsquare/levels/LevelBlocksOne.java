package com.hulponot.games.fallingsquare.levels;

import android.util.Log;
import android.util.Pair;
import android.widget.TextView;

import com.hulponot.games.fallingsquare.MyGLRenderer;
import com.hulponot.games.fallingsquare.R;
import com.hulponot.games.fallingsquare.cube.BackgroundCubeRenderer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by hulpo on 6/12/2017.
 */

public class LevelBlocksOne extends LevelBlocks {
    private final LinkedList<LinkedList<Room>> levels = new LinkedList<>();
    private final LinkedList<Integer> speedsForWin = new LinkedList<>();

    public LevelBlocksOne(MyGLRenderer renderer, BackgroundCubeRenderer backgroundCubeRenderer, int num) {
        super(renderer, backgroundCubeRenderer, 777, num);

        Scanner scanner = null;

        RoomRenderer roomRenderer = new SimpleRoomRenderer(renderer, backgroundCubeRenderer);

        //levels initialization
        try {
            scanner = new Scanner(
                    new InputStreamReader(renderer.getActivity().getAssets().open("levels.data")));
            while (scanner.hasNextInt()) {
                int levelsNum = scanner.nextInt();
                for (int level = 0; level < levelsNum; level++) {
                    float offset = -3.0f;
                    int roomsNum = scanner.nextInt();
                    speedsForWin.add(scanner.nextInt());

                    LinkedList<Room> levelOne = new LinkedList();
                    for (int room = 0; room < roomsNum; room++) {
                        int exit = scanner.nextInt();
                        int high = scanner.nextInt();
                        levelOne.add(new SimpleRoom(roomRenderer, exit, high));
                    }
                    levels.add(levelOne);
                }
            }
        } catch (java.io.IOException e) {
            Log.e("LEVELS NOT LOADED", "LevelBlocksOne: cant open file");
        } finally {
            if (scanner != null)
                scanner.close();
        }

        if (num >= 0 && num < levels.size()) {
            rooms = levels.get(num);
            speedForWin = speedsForWin.get(num);
        }
    }

    public void nextLevel() {
        tmpRoom = 1;
        roomStart = 0;
        restart();
        if (levels.size() > ++levelNum) {
            Log.d("LEVELNUM", "nextLevel: " + levelNum);
            rooms = levels.get(levelNum);
            speedForWin = speedsForWin.get(levelNum);

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) activity.findViewById(R.id.maxSpeed)).setText(String.valueOf(speedForWin));
                }
            });
        }
    }

    public boolean isLast() {
        if (levels.size() > 1 + levelNum)
            return false;
        else
            return true;

    }
}
