package com.hulponot.games.fallingsquare.levels;


import android.widget.TextView;

import com.hulponot.games.fallingsquare.InternalActivity;
import com.hulponot.games.fallingsquare.MyGLRenderer;
import com.hulponot.games.fallingsquare.R;
import com.hulponot.games.fallingsquare.cube.BackgroundCubeRenderer;
import com.hulponot.games.fallingsquare.cube.CubeRenderer;

import java.text.DecimalFormat;
import java.text.Format;
import java.util.Locale;
import java.util.Random;

import java.util.ArrayList;


/**
 * Created by hulpo on 6/26/2017.
 */

public class LevelBlocksInternal extends LevelBlocks {
    ArrayList<Room> roomPool = new ArrayList<>();
    Room firstRoom;
    Random rnd;

    public double getScores() {
        return scores;
    }

    float scores = 0;

    public LevelBlocksInternal(MyGLRenderer renderer, BackgroundCubeRenderer backgroundCubeRenderer) {
        super(renderer, backgroundCubeRenderer, 9000, -2);

        scores = ((InternalActivity)renderer.getActivity()).getScore();
        rnd = new Random();
        RoomRenderer roomRenderer = new SimpleRoomRenderer(renderer, backgroundCubeRenderer);

        for (int i = 0; i < 42; i++) {
            roomPool.add(new SimpleRoom(roomRenderer, Math.abs(rnd.nextInt()) % 8, 8 + Math.abs(rnd.nextInt()) % 12));
        }

        firstRoom = new SimpleRoom(roomRenderer, -1, RoomRenderer.cubeScale);
        rooms.add(firstRoom);
        for (int i = 0; i < 10; i++) {
            rooms.addLast(roomPool.get(Math.abs(rnd.nextInt()) % roomPool.size()));
        }
    }

    @Override
    public void move(float offset, float playerHorizontalOffset) {
        super.move(offset, playerHorizontalOffset);

        if (renderer.getSpeed() > renderer.MIN_SPEED)
            scores += renderer.getSpeed() - renderer.MIN_SPEED;

        renderer.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView scoreView = (TextView) renderer.getActivity().findViewById(R.id.internalScore);
                scoreView.setText(String.format(Locale.ENGLISH, "%.02f", scores));
            }
        });
    }

    @Override
    public void nextRoom() {
        if (tmpRoom > 3) {
            roomStart += rooms.get(tmpRoom).getHigh();
            rooms.removeFirst();
            rooms.addLast(roomPool.get(Math.abs(rnd.nextInt()) % roomPool.size()));
        } else {
            super.nextRoom();
        }
    }

    @Override
    public void restart() {
        super.restart();

        scores = 0;
        rooms.removeFirst();
        rooms.addFirst(firstRoom);
    }
}
