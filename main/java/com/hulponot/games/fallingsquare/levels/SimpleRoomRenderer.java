package com.hulponot.games.fallingsquare.levels;

import com.hulponot.games.fallingsquare.MyGLRenderer;
import com.hulponot.games.fallingsquare.cube.BackgroundCubeRenderer;

/**
 * Created by hulpo on 6/16/2017.
 */

public class SimpleRoomRenderer extends RoomRenderer {
    public SimpleRoomRenderer(MyGLRenderer renderer, BackgroundCubeRenderer backgroundCubeRenderer) {
        super(renderer, backgroundCubeRenderer);
    }

    @Override
    public void draw(float[] MVP, long time,float high, boolean focus, int exit) {
        super.draw(MVP, time, high, focus, exit);
    }
}
