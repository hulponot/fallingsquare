package com.hulponot.games.fallingsquare.levels;

import com.hulponot.games.fallingsquare.Cube;
import com.hulponot.games.fallingsquare.MyGLRenderer;
import com.hulponot.games.fallingsquare.cube.BackgroundCubeRenderer;
import com.hulponot.games.fallingsquare.cube.LevelCubeMovement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hulpo on 6/16/2017.
 */

abstract class RoomRenderer {
    protected List<Cube> blockList = new ArrayList<>();
    protected Cube veil;
    protected Cube light;
    protected Cube lWall;
    protected Cube rWall;
    protected final float exitColor[] = new float[]{1.0f, 1.0f, 0.5f, 0.77f};
    protected final float exitColorHidden[] = new float[]{1.0f, 1.0f, 0.0f, 0.00f};
    MyGLRenderer renderer;
    public static float cubeScale = 2.0f;

    public RoomRenderer(MyGLRenderer renderer, BackgroundCubeRenderer backgroundCubeRenderer) {
        this.renderer = renderer;

        for (int column = 0; column < 8; column++) {
            blockList.add(
                    new Cube(
                            new LevelCubeMovement(new float[]{(-3.5f + column) * cubeScale, cubeScale / 2, 0.0f}),
                            backgroundCubeRenderer,
                            renderer,
                            new float[]{1.0f, 1.0f, 1.0f, 1.0f})
            );
        }
        veil = new Cube(
                new LevelCubeMovement(new float[]{0, cubeScale / 2, 0}),
                backgroundCubeRenderer,
                renderer,
                new float[]{0.0f, 0.0f, 0.0f, 0.42f}
        );

        light = new Cube(
                new LevelCubeMovement(new float[]{0, 0.1f, 0}),
                backgroundCubeRenderer,
                renderer,
                new float[]{1.0f, 1.0f, 0.0f, 0.88f}
        );
        lWall = new Cube(
                new LevelCubeMovement(new float[]{0, 0, 0}),
                backgroundCubeRenderer,
                renderer,
                new float[]{0.811f, 0.443f, 0.384f, 0.8f}
        );
        rWall = new Cube(
                new LevelCubeMovement(new float[]{0, 0 , 0}),
                backgroundCubeRenderer,
                renderer,
                new float[]{0.811f, 0.443f, 0.384f, 0.8f}
        );
    }

    public void draw(float MVP[], long time, float high, boolean focus, int exit) {
        for (int i = 0; i < blockList.size(); i++) {
            if (i == exit && focus) {
                if (renderer.isPlayerSafe()) {
                    blockList.get(i).draw(MVP, exitColorHidden,time, false);
                } else {
                    blockList.get(i).draw(MVP, exitColor,time, false);
                }
            } else {
                blockList.get(i).draw(MVP, time, false);
            }
        }

        if (!focus) {
            veil.getMovement().scale(8 * cubeScale, high, 2);
            veil.getMovement().translateTo(0, high / 2, 0);
            veil.draw(MVP, time, false);
        } else if (exit >= 0){
            light.setColor(LevelBlocks.lightColor);
            light.getMovement().scale(0.2f, high, 0.2f);
            light.getMovement().translateTo((-4 + exit + 0.5f) * cubeScale, high / 2, 0);
            light.draw(MVP, time, true);
        }

        if (renderer.getDirection()){
            rWall.setColorPart(3, 0.77f);
            lWall.setColorPart(3, 0f);
        } else {
            rWall.setColorPart(3, 0f);
            lWall.setColorPart(3, 0.77f);
        }
        rWall.getMovement().scale(0.2f, high, 0.2f);
        rWall.getMovement().translateTo(4*cubeScale, high / 2, 0);
        rWall.draw(MVP, time, true);

        lWall.getMovement().translateTo(-4*cubeScale, high / 2, 0);
        lWall.getMovement().scale(0.2f, high, 0.2f);
        lWall.draw(MVP, time, true);
    }
}
