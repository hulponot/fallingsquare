package com.hulponot.games.fallingsquare;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hulponot.games.fallingsquare.R;

import java.util.LinkedList;

public class LevelsFragment extends Fragment {

    private LinkedList<Integer> levelIds;
    private LinkedList<Integer> levelBtnIds;

    public LevelsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        levelIds = new LinkedList<>();
        levelIds.add(R.id.lvlLock1);
        levelIds.add(R.id.lvlLock2);
        levelIds.add(R.id.lvlLock3);
        levelIds.add(R.id.lvlLock4);
        levelIds.add(R.id.lvlLock5);

        levelBtnIds = new LinkedList<>();
        levelBtnIds.add(R.id.lvlBtn1);
        levelBtnIds.add(R.id.lvlBtn2);
        levelBtnIds.add(R.id.lvlBtn3);
        levelBtnIds.add(R.id.lvlBtn4);
        levelBtnIds.add(R.id.lvlBtn5);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d("HIGHLEVEL", "onResume: " + ((MenuActivity) getActivity()).getHighLevel());

        getActivity().findViewById(R.id.lvlLock1).bringToFront();
        getActivity().findViewById(R.id.lvlLock1).requestLayout();
        for (int i = 0; i < levelIds.size(); i++)
            if ((i + 1) > ((MenuActivity) getActivity()).getHighLevel()) {
                getActivity().findViewById(levelIds.get(i)).setVisibility(View.VISIBLE);
                getActivity().findViewById(levelBtnIds.get(i)).setVisibility(View.INVISIBLE);
            } else {
                getActivity().findViewById(levelIds.get(i)).setVisibility(View.INVISIBLE);
                getActivity().findViewById(levelBtnIds.get(i)).setVisibility(View.VISIBLE);
            }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_levels, container, false);
    }
}
