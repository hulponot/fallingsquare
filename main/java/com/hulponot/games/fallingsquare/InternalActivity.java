package com.hulponot.games.fallingsquare;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Locale;

public class InternalActivity extends Activity {
    private float score;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal);

        updateMaxScoreView();

        mInterstitialAd = new InterstitialAd(this);
        //mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.setAdUnitId(getString(R.string.admob_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences scores = getSharedPreferences(getString(R.string.pref_scores), MODE_PRIVATE);
        score = scores.getFloat(getString(R.string.tmp_scores), 0);
    }

    @Override
    protected void onPause() {
        super.onPause();

        updateScores();
    }

    public void updateScores(){
        SharedPreferences scores = getSharedPreferences(getString(R.string.pref_scores), MODE_PRIVATE);
        float highest_score = scores.getFloat(getString(R.string.high_score), 0);

        TextView score = (TextView)findViewById(R.id.internalScore);
        float high_score = Float.valueOf(score.getText().toString());

        SharedPreferences.Editor editor = scores.edit();
        editor.putFloat(getString(R.string.tmp_scores), high_score);
        if(highest_score < high_score){
            editor.putFloat(getString(R.string.high_score), high_score);
        }
        editor.commit();
    }

    public void updateMaxScoreView(){
        SharedPreferences scores = getSharedPreferences(getString(R.string.pref_scores), MODE_PRIVATE);
        float highest_score = scores.getFloat(getString(R.string.high_score), 0);

        TextView maxScore = (TextView)findViewById(R.id.internalMaxScore);
        maxScore.setText(String.format(Locale.ENGLISH, "%.02f", highest_score));
    }

    public void backToMenu(View view){
        NavUtils.navigateUpFromSameTask(this);
    }

    public float getScore(){return score;}

    public void showAd(){
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("ADEBUG", "The interstitial wasn't loaded yet.");
        }
    }
}
