package com.hulponot.games.fallingsquare;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class GameActivity extends Activity {

    public static String LEVEL_TO_BE_LOADED = "com.hulponot.games.fallingsquare.LEVEL_TO_BE_LOADED";
    private int levelStart;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        levelStart = getIntent().getIntExtra(LEVEL_TO_BE_LOADED, 1);
        setContentView(R.layout.activity_game);

        mInterstitialAd = new InterstitialAd(this);
        //mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.setAdUnitId(getString(R.string.admob_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    public int getLevelStart(){return levelStart;}

    public void openLevel(int newLevel){
        SharedPreferences pref = getSharedPreferences(getString(R.string.pref_scores), MODE_PRIVATE);
        float highest_level = pref.getInt(getString(R.string.high_level), 1);

        Log.d("HIGHLEVEL", "openLevel: " + newLevel + " tmp " + highest_level);
        if(highest_level < newLevel){
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(getString(R.string.high_level), newLevel);
            editor.commit();
        }
    }
    public void backToMenu(View view){
        NavUtils.navigateUpFromSameTask(this);
    }

    public void showAd(){
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("ADEBUG", "The interstitial wasn't loaded yet.");
        }
    }

    public void showLevel(int levelNum) {
        ((TextView)findViewById(R.id.level_num)).setText("level " + (levelNum+1) );
    }

    public void showWin(boolean visible){
        TextView winText = (TextView)findViewById(R.id.winNotify);
        winText.setText(getString(R.string.win_notify));
        winText.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    public void showAllComplete(boolean visible){
        TextView winText = (TextView)findViewById(R.id.winNotify);
        winText.setText(getString(R.string.complete_notify));
        winText.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }
}
