package com.hulponot.games.fallingsquare;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

public class TutorialActivity extends Activity {

    public static List<String> phrases;
    public static List<String> shortPhrases;
    public static List<Integer> conditions;
    public Integer step = 0;
    TextView tutorial_speech;
    Button tutorial_overlaps;

    public Integer getCondition() {
        return conditions.get(step);
    }

    public void nextStep() {
        if (phrases.size() > 1+step) {
            tutorial_overlaps.setText(phrases.get(++step));
            tutorial_overlaps.setVisibility(View.VISIBLE);
        } else
            NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        phrases = new LinkedList<>();
        shortPhrases = new LinkedList<>();
        conditions = new LinkedList<>();

        phrases.add("Hello!");
        shortPhrases.add("empty");
        conditions.add(0);

        phrases.add("In this game you manage rectangle. You can make it falling by touching a screen");
        shortPhrases.add("Touch the game window");
        conditions.add(0);

        phrases.add("Perfect! There is three directions to move. Left, right, down.");
        shortPhrases.add("empty");
        conditions.add(0);

        phrases.add("You can choose direction, rectangle will fly after falling");
        shortPhrases.add("empty");
        conditions.add(0);

        phrases.add("Your current speed shown on top right side of game screen");
        shortPhrases.add("empty");
        conditions.add(0);

        phrases.add("Each level has speed which should be attained");
        shortPhrases.add("empty");
        conditions.add(0);

        phrases.add("You can't fall through other cubes, except those with an yellow light on");
        shortPhrases.add("fall through first row of cubes");
        conditions.add(1);

        phrases.add("Now try to complete level by accelerating up to 15");
        shortPhrases.add("ACCELERATE!");
        conditions.add(3);

        phrases.add("Perfect! Now, you are ready!");
        shortPhrases.add("empty");
        conditions.add(5);

        setContentView(R.layout.activity_tutorial);

        tutorial_speech = (TextView) findViewById(R.id.tutorialSpeech);
        tutorial_overlaps = (Button) findViewById(R.id.tutorOverlaps);

        tutorial_overlaps.setText(phrases.get(step));
    }

    public void nextStep(View view) {
        if (shortPhrases.get(step).equals("empty"))
            nextStep();
        else {
            tutorial_overlaps.setVisibility(View.INVISIBLE);
            tutorial_speech.setText(shortPhrases.get(step));
        }
    }
}
