package com.hulponot.games.fallingsquare;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;

public class MenuActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    LevelsFragment levelsFragment;
    MenuFragment menuFragment;
    private int highLevel;
    GoogleApiClient mGoogleApiClient;

    private boolean mResolvingConnectionFailure = false;
    private boolean mAutoStartSignInflow = true;
    private boolean mSignInClicked = false;

    private static int RC_SIGN_IN = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        menuFragment = new MenuFragment();
        levelsFragment = new LevelsFragment();

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, menuFragment).commit();

        //MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");
        MobileAds.initialize(this, getString(R.string.admob_app_id));
    }

    @Override
    protected void onResume() {
        super.onResume();


        SharedPreferences pref = getSharedPreferences(getString(R.string.pref_scores), MODE_PRIVATE);
        highLevel = pref.getInt(getString(R.string.high_level), 1);
    }

    public void startGame(View view) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, levelsFragment).addToBackStack(null).commit();
    }

    public void startInternalGame(View view) {
        Intent intent = new Intent(this, InternalActivity.class);
        startActivity(intent);
    }

    public void backToMenu(View view) {
        getSupportFragmentManager().popBackStack();
        //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, menuFragment).commit();
    }

    public void startTutorial(View view) {
        Intent intent = new Intent(this, TutorialActivity.class);
        startActivity(intent);
    }

    public void startLevel(View view) {
        Integer levelToLoad = Integer.valueOf(((TextView) view).getText().toString());
        if (levelToLoad <= highLevel) {
            Intent intent = new Intent(this, GameActivity.class);
            intent.putExtra(GameActivity.LEVEL_TO_BE_LOADED, levelToLoad-1);
            startActivity(intent);
        }
    }

    public int getHighLevel() {
        return highLevel;
    }

    public void apiSignIn(View view) {
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                    // add other APIs and scopes here as needed
                    .build();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mGoogleApiClient.connect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {

        }
    }
}
